<?php

namespace Database\Factories;

use App\Models\Reservation;
use Illuminate\Database\Eloquent\Factories\Factory;


class ReservationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reservation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $name,
            'apellido' => $apellido,
            'detalle' => $detalle,
            'body' => $this->faker->paragraph(rand(200,500)),
            'precio' => $this->faker->paragraph(rand(200,500)),
            'user_id' => $this->faker->randomDigit(1,10),
            'product_id' =>$this->faker->randomDigit(1,10),
        ];
    }
}
