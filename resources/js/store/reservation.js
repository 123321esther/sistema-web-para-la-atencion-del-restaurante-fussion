import axios from 'axios'

export default {
    namespaced:true,
    state:{
        allreservations:[]
    },
    getters:{
        get_all_reservations(state){
            return state.allreservations
        }
    },
    mutations:{
       SET_ALL_RESERVATIONS(state,value){
            state.allreservations = value
       }
    },
    actions:{
        async getReservationsAll({dispatch}){
            console.log('solicitando todas las reservaciones')
            await axios.get('/sanctum/csrf-cookie')
            const ct = await axios.get('/api/reservations')
            return dispatch('meReservation',ct.data)
        },
        async createReservation({dispatch},newReservation){
            console.log('solicitando la creacion de una nueva reservacion')
            console.log(newReservation)
            //return false
            await axios.get('/sanctum/csrf-cookie')
            await axios.post('/api/reservation',newReservation)
        },
        meReservation({commit},data){
            console.log(data)
            commit('SET_ALL_RESERVATIONS',data)
        }
    }
}
