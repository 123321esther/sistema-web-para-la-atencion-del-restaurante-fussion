import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import post from './post'
import category from './category'
import reservation from './reservation'
import comment from './reservation'

Vue.use(Vuex)
export default new Vuex.Store({
    modules:{
        auth,
        post,
        category, 
        reservation, 
        comment,  
        //aqui los demas modulos
    }
})

