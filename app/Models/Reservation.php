<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'body',
        'apellido',
        'precio',
        'detalle',
        'products_id',
        'user_id'
        ];

    public function posts(){
        //devolveme todas las publicaciones con esta categoria
        return $this->hasMany(Post::class);
    }
    public function products(){
        //devolveme todas las publicaciones con esta categoria
        return $this->hasMany(Products::class);
    }

    public function getRouterKeyName(){//ID slug
        return 'slug';
    }
}
