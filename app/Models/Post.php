<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//

use App\Models\User;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Reservation;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'body',
        'category_id',
        'user_id',
        'photo'
    ];

    public function user(){
        //devolveme al ususario que creo esta publicacion
        return $this->belongsTo(User::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function comments(){
        return $this->hasMany(comment::class);
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopeLatest($query){
        return $query->orderBy('created_at','DESC');
    }
    public function reservation(){
        return $this->belongsTo(Reservation::class);
    }
}

