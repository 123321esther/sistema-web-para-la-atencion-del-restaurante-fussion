<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Category;
use App\Models\Comment;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'slug',
        'body',
        'precio',
        'category_id',
        'user_id',
        'photo'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function comments(){
        return $this->belongsTo(Category::class);
    }

    public function category(){
        return $this->hasMany(Comment::class);
    }

    public function getRouteKeyName(){
        return 'slug';

    }

    public function scopeLatest($query){
        return $query->orderBy('created_at','DESC');
    }
}
