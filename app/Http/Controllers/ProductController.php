<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd('aqui llega');
        //
        if($request['file']){
            $imageName = time().'.'.$request['file']->getClientOriginalExtension();
            //dd($imageName);
            $data = json_decode($request['form']);
            //dd($data['body']);
            //dd($data->title);
            Product::create([
                'title' => $data->title,
                'slug' => Str::slug($data->title),
                'body' => $data->body,
                'precio' => $data->precio,
                'category_id' => $data->category,
                'user_id' => Auth::user()->id,
                'photo' => '/uploads/'.$imageName
            ]);
            $request['file']->move(public_path('uploads'),$imageName);
    
            return response()->json(['state'=>'Exito de creacion de publicacion'], 200);
        }else{
            return response()->json(['state'=>'No existe imagen para la publicacion'], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
